# Qismo Jira
Sebuah package untuk mengintegrasikan Jira app dengan Qiscus Multichannel. Beberapa fitur yang sampai saat ini didukung antara lain:
- Create issue menggunakan fitur Custom Button di Qiscus Multichannel
- Store link to Jira issue in room additional info
- Close issue if room in Qiscus Multichannel is resolved
- Store chat history in Jira issue's comment
- Store notes in Jira issue's comment

### Requirements
- Go 1.18
- Qiscus Multichannel Account

### Local Setup
**Clone Repository**

```bash
git clone git@bitbucket.org:qiscus/qismo-jira.git
cd `qismo-jira`
```

**Install Packages**
```bash
go mod tidy
```

**Setup Environment Variables**
> Please check the `.env.sample` for the description of each variables
```bash
cp .env.sample .env
```
**Run Test**

```bash
go test ./...
```

**Run Service**
```bash
go run cmd/main.go
```
