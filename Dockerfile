# Builder Stage
FROM golang:1.18.2-alpine AS builder

RUN apk update
RUN apk add --no-cache curl bash nano

ARG VERSION
ARG CGO_ENABLED

WORKDIR /app/src
COPY . .

RUN go mod tidy
RUN go build -ldflags "-s -w -X main.version=${VERSION}" -o /app/qismo-jira ./cmd

WORKDIR /app

RUN rm -rf src/
ENTRYPOINT ["/app/qismo-jira"]


# Production Stage
FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=builder /app/qismo-jira ./app
CMD ["./app"]
