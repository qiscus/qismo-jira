package api

import (
	"net/http"
	"qismo-jira/config"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
)

type API struct {
	Conf config.Config
}

func NewAPI(conf *config.Config) API {
	return API{
		Conf: *conf,
	}
}

func (a *API) Home(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"ok": true,
	})
}

func (a *API) RouteNotFound(c *gin.Context) {
	c.JSON(http.StatusNotFound, gin.H{
		"message": "resource not found",
	})
}

func (a *API) AbortWebhookHandling(c *gin.Context, message string) {
	c.JSON(http.StatusBadRequest, gin.H{
		"ok":      false,
		"message": message,
	})
}

func (a *API) AbortWebhookHandlingStr(c *gin.Context, message string) {
	c.String(http.StatusBadRequest, message)
}

func printErrorLogWithRequestId(requestId, message string) {
	log.Error().Msgf("%s - %s", requestId, message)
}
