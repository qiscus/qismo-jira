package api

import (
	"fmt"
	"net/http"
	"qismo-jira/config"
	"qismo-jira/jiraq"
	"qismo-jira/params"
	"qismo-jira/qismo"
	"strings"

	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
	"github.com/samber/lo"
)

func (a *API) HandleMarkAsResolvedWebhook(c *gin.Context) {
	var p params.MarkAsResolvedWebhook
	requestIdStr := requestid.Get(c)

	err := c.ShouldBindJSON(&p)
	if err != nil {
		printErrorLogWithRequestId(requestIdStr, fmt.Sprintf("failed to parse mark as resolved webook. %s", err.Error()))
		a.AbortWebhookHandlingStr(c, fmt.Sprintf("failed to parse mark as resolved webhook. %s", err.Error()))
		return
	}

	if len(p.Customer.AdditionalInfo) < 1 {
		printErrorLogWithRequestId(requestIdStr, "webhook ignored. room doesnt has user properties")
		a.AbortWebhookHandlingStr(c, "ignore webhoook. room doesnt has user properties")
		return
	}

	linkAi, linkAiFound := lo.Find(p.Customer.AdditionalInfo, func(ai params.AdditionalInfo) bool {
		return ai.Key == "Jira Issue Link"
	})

	if !linkAiFound {
		printErrorLogWithRequestId(requestIdStr, "webhook ignored. room doesnt has jira issue link additional info")
		a.AbortWebhookHandlingStr(c, "ignore webhoook. room doesnt has jira issue link")
		return
	}

	link := linkAi.Value
	linkSplit := strings.Split(link, "/")
	issueKey := linkSplit[len(linkSplit)-1]

	if issueKey == "" {
		printErrorLogWithRequestId(requestIdStr, "webhook ignored. jira issue link is blank")
		a.AbortWebhookHandlingStr(c, "webhook ignored. jira issue link in additional info is blank or null")
		return
	}

	if !strings.HasPrefix(issueKey, a.Conf.JiraProjectKey) {
		printErrorLogWithRequestId(requestIdStr, "ignore webhook. jira issue link is invalid. not started with project key")
		a.AbortWebhookHandlingStr(c, "webhook ignored. jira issue link in additional info is invalid. not started with project key")
		return
	}

	go handleMarkAsResolvedWebhookInBackground(
		requestIdStr,
		issueKey,
		a.Conf,
		p,
	)

	c.String(http.StatusOK, "mark as resolved webhook is being processed!")
}

func handleMarkAsResolvedWebhookInBackground(requestIdStr string, issueKey string, conf config.Config, p params.MarkAsResolvedWebhook) {
	jiraClient, err := jiraq.New(conf.JiraUsername, conf.JiraToken, conf.JiraURL, conf.JiraProjectKey)
	if err != nil {
		printErrorLogWithRequestId(requestIdStr, fmt.Sprintf("failed to init jira client. %s", err.Error()))
		return
	}

	qismoClient := qismo.New(conf.QiscusAppID, conf.QiscusSecretKey, conf.QismoBaseURL, conf.QiscusBaseURL)

	err = jiraClient.CloseIssue(issueKey)
	if err != nil {
		printErrorLogWithRequestId(requestIdStr, fmt.Sprintf("failed to close ticket. %s", err.Error()))
		return
	}

	qismoClient.PostCustomSystemEventMessage(p.Service.RoomID, fmt.Sprintf("Successfully closed Jira issue %s", issueKey))

	if p.Service.Notes != "" {
		err = jiraClient.AddRichTextComment(issueKey, "Notes", p.Service.Notes)
		printErrorLogWithRequestId(requestIdStr, fmt.Sprintf("failed to add notes. %s", err.Error()))
	}

	if p.Service.LastCommentID != "" {
		comments, err := qismoClient.LoadAllCommentsInRoom(p.Service.RoomID, p.Service.LastCommentID)
		if err != nil {
			printErrorLogWithRequestId(requestIdStr, fmt.Sprintf("failed to load comments. %s", err.Error()))
			return
		}

		messageForExport := qismoClient.BuildMessagesForExport(comments)

		err = jiraClient.AddRichTextComment(issueKey, "Chat History", messageForExport)
		if err != nil {
			printErrorLogWithRequestId(requestIdStr, fmt.Sprintf("failed to add chat history. %s", err.Error()))
			return
		}
	}
}
