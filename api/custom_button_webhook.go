package api

import (
	"fmt"
	"net/http"
	"qismo-jira/config"
	"qismo-jira/jiraq"
	"qismo-jira/params"
	"qismo-jira/qismo"

	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
)

func (a *API) HandleCustomButtonWebhook(c *gin.Context) {
	var p params.CustomButtonWebhook
	err := c.ShouldBindJSON(&p)
	if err != nil {
		a.AbortWebhookHandlingStr(c, fmt.Sprintf("failed to parse custom button webhook. %s", err.Error()))
		return
	}

	go handleCustomButtonWebhook(requestid.Get(c), a.Conf, p)
	c.String(http.StatusOK, "Proses pembuatan tiket sedang diproses, harap tunggu sebentar")
}

func handleCustomButtonWebhook(requestIdStr string, conf config.Config, p params.CustomButtonWebhook) {
	qismoClient := qismo.New(conf.QiscusAppID, conf.QiscusSecretKey, conf.QismoBaseURL, conf.QiscusBaseURL)

	jiraClient, err := jiraq.New(conf.JiraUsername, conf.JiraToken, conf.JiraURL, conf.JiraProjectKey)
	if err != nil {
		printErrorLogWithRequestId(requestIdStr, "failed initiate Jira client")
		qismoClient.PostCustomSystemEventMessage(p.RoomIdStr(), "Gagal menginisiasi Jira client!")
		return
	}

	assigneeId := ""
	agentInJira, err := jiraClient.FindUserByEmail(p.Agent.Email)
	if err != nil {
		printErrorLogWithRequestId(requestIdStr, fmt.Sprintf("failed to get assignee data. %s", err.Error()))
	}

	if agentInJira != nil {
		assigneeId = agentInJira.AccountID
	}

	issueOptions := jiraq.CreateIssueOptions{
		Summary:              fmt.Sprintf("Qiscus inquiry from room %s", p.RoomIdStr()),
		Description:          fmt.Sprintf("Custom from room %s has a problem", p.RoomIdStr()),
		MultichannelRoomLink: qismoClient.GetDashboardLinkForRoomId(p.RoomIdStr()),
		CustomerName:         p.Customer.Name,
		AssigneeId:           assigneeId,
	}

	if len(p.Tags) >= 1 {
		var labels []string

		for _, v := range p.Tags {
			labels = append(labels, v.Name)
		}

		issueOptions.Labels = labels
	}

	issue, err := jiraClient.CreateIssue(&issueOptions)
	if err != nil {
		printErrorLogWithRequestId(requestIdStr, fmt.Sprintf("failed to create issue in Jira. %s", err.Error()))
		qismoClient.PostCustomSystemEventMessage(p.RoomIdStr(), "Gagal membuat issue di Jira")
		return
	}

	issueLink := fmt.Sprintf("%sbrowse/%s", jiraClient.URL, issue.Key)

	qismoClient.UpdateRoomAdditionalInfo(p.RoomIdStr(), []qismo.UserProperties{
		{
			Key:   "Jira Issue Link",
			Value: issueLink,
		},
	})

	qismoClient.PostCustomSystemEventMessage(
		p.RoomIdStr(),
		"Jira issue has been created. Optionally, please reload this page to see issue link in additional info",
	)
}
