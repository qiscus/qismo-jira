package params

type MarkAsResolvedWebhook struct {
	Service  Service  `json:"service"`
	Customer Customer `json:"customer"`
}
type Service struct {
	FirstCommentID string `json:"first_comment_id"`
	LastCommentID  string `json:"last_comment_id"`
	Notes          string `json:"notes"`
	RoomID         string `json:"room_id"`
}
