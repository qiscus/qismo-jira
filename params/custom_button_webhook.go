package params

import "strconv"

type CustomButtonWebhook struct {
	AdditionalInfo     []AdditionalInfo     `json:"additional_info"`
	Agent              Agent                `json:"agent"`
	ChannelID          int                  `json:"channel_id"`
	ChannelName        string               `json:"channel_name"`
	ChannelType        string               `json:"channel_type"`
	Customer           Customer             `json:"customer"`
	CustomerProperties []CustomerProperties `json:"customer_properties"`
	Notes              string               `json:"notes"`
	RoomID             int                  `json:"room_id"`
	Tags               []Tag                `json:"tag"`
}
type AdditionalInfo struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
type Agent struct {
	Email string `json:"email"`
	Name  string `json:"name"`
	Type  string `json:"type"`
}
type Customer struct {
	AdditionalInfo []AdditionalInfo `json:"additional_info"`
	Avatar         string           `json:"avatar"`
	Name           string           `json:"name"`
	UserID         string           `json:"user_id"`
}
type CustomerProperties struct {
	ID    int    `json:"id"`
	Label string `json:"label"`
	Value string `json:"value"`
}
type Tag struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (w *CustomButtonWebhook) RoomIdStr() string {
	return strconv.Itoa(w.RoomID)
}
