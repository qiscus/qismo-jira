module qismo-jira

// +heroku goVersion go1.18
go 1.18

require (
	github.com/gin-contrib/requestid v0.0.1
	github.com/gin-gonic/gin v1.7.7
)

require (
	github.com/joho/godotenv v1.4.0
	github.com/kelseyhightower/envconfig v1.4.0
)

require (
	github.com/go-resty/resty/v2 v2.7.0 // indirect
	github.com/samber/lo v1.25.0 // indirect
	golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
	golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
)

require (
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/go-playground/locales v0.14.0 // indirect
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/rs/zerolog v1.26.1
	github.com/ugorji/go/codec v1.2.6 // indirect
	golang.org/x/crypto v0.0.0-20211215165025-cf75a172585e // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/text v0.3.7 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

require (
	github.com/andygrunwald/go-jira v1.15.1
	github.com/fatih/structs v1.1.0 // indirect
	github.com/golang-jwt/jwt/v4 v4.3.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/trivago/tgo v1.0.7 // indirect
)
