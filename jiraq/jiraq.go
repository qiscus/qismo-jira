package jiraq

import (
	"fmt"

	"github.com/andygrunwald/go-jira"
	"github.com/samber/lo"
	"github.com/trivago/tgo/tcontainer"
)

const (
	InProgressTransitionId = "21"
	DoneTransitionId       = "31"
	JiraIssueTypeId        = "10004"
)

type JiraqClient struct {
	jiraClient *jira.Client

	// Email user sebagai admin
	Username string

	// Access token yang didapatkan dari halaman profile. Harus sebagai admin
	Token string

	// Example URL: "https://qismo-jira.atlassian.net/"
	URL string

	// Project key. Example: NAB
	ProjectKey string

	// Memoize custom fields
	customFields []jira.Field
}

func New(username, token, url, projectKey string) (*JiraqClient, error) {
	client := new(JiraqClient)

	tp := jira.BasicAuthTransport{
		Username: username,
		Password: token,
	}

	jiraClient, err := jira.NewClient(tp.Client(), url)
	if err != nil {
		return nil, err
	}

	client.jiraClient = jiraClient
	client.Username = username
	client.Token = token
	client.URL = url
	client.ProjectKey = projectKey

	return client, nil
}

func (c *JiraqClient) FindUserByEmail(email string) (*jira.User, error) {
	users, _, err := c.jiraClient.User.Find(email)
	if err != nil {
		return nil, err
	}

	if len(users) < 1 {
		return nil, fmt.Errorf("user with email %s not found", email)
	}

	user := users[0]

	return &user, err
}

func (c *JiraqClient) ListCustomFields() ([]jira.Field, error) {
	customFields := c.customFields
	if len(customFields) > 0 {
		return customFields, nil
	}

	customFields, _, err := c.jiraClient.Field.GetList()
	if err != nil {
		return customFields, err
	}

	c.customFields = customFields

	return c.customFields, nil
}

func (c *JiraqClient) GetCustomFieldByName(name string) (*jira.Field, error) {
	customFields, err := c.ListCustomFields()
	if err != nil {
		return nil, err
	}

	field, fieldFound := lo.Find(customFields, func(f jira.Field) bool {
		return f.Name == name
	})

	if !fieldFound {
		return nil, fmt.Errorf("`%s` custom field is not found", name)
	}

	return &field, nil
}

type CreateIssueOptions struct {
	Summary              string
	Description          string
	AssigneeId           string
	CustomerName         string
	MultichannelRoomLink string
	Labels               []string
}

func (c *JiraqClient) ListIssueAvailableTransitions(issueId string) ([]jira.Transition, error) {
	transitions, _, err := c.jiraClient.Issue.GetTransitions(issueId)
	return transitions, err
}

func (c *JiraqClient) CreateIssue(opt *CreateIssueOptions) (*jira.Issue, error) {
	customFieldsContainer := makeCustomFieldsContainer()

	customerNameField, err := c.GetCustomFieldByName("Customer Name")
	if err == nil {
		customFieldsContainer[customerNameField.ID] = opt.CustomerName
	}

	multichannelRoomLink, err := c.GetCustomFieldByName("Multichannel Room Link")
	if err == nil {
		customFieldsContainer[multichannelRoomLink.ID] = opt.MultichannelRoomLink
	}

	issueField := jira.IssueFields{
		Summary:     opt.Summary,
		Description: opt.Description,
		Project:     jira.Project{Key: c.ProjectKey},
		Assignee:    &jira.User{AccountID: opt.AssigneeId},
		Unknowns:    customFieldsContainer,
		Type:        jira.IssueType{ID: JiraIssueTypeId},
	}

	if len(opt.Labels) >= 1 {
		issueField.Labels = opt.Labels
	}

	issue, _, err := c.jiraClient.Issue.Create(&jira.Issue{
		Fields:      &issueField,
		Transitions: []jira.Transition{{ID: InProgressTransitionId}},
	})

	if err != nil {
		return nil, err
	}

	_, err = c.jiraClient.Issue.DoTransition(issue.ID, InProgressTransitionId)
	if err != nil {
		fmt.Printf("Error while mark issue as in progress. %s", err.Error())
	}

	return issue, nil
}

func (c *JiraqClient) CloseIssue(issueId string) error {
	_, err := c.jiraClient.Issue.DoTransition(issueId, DoneTransitionId)
	return err
}

func (c *JiraqClient) RunCloseIssuePipeline(issueKey, notes, chatHistory string) error {
	err := c.CloseIssue(issueKey)
	if err != nil {
		return err
	}

	c.AddRichTextComment(issueKey, "Notes", notes)
	c.AddRichTextComment(issueKey, "Chat History", chatHistory)

	return nil
}

// The go-jira package doesn't have a wrapper to add comments to Jira API v3 yet.
// So, if you want to add comments with rich text formatted, then we need to use
// `NewRequest` and `Do` API directly
func (c *JiraqClient) AddRichTextComment(issueKey, heading, paragraph string) error {
	path := fmt.Sprintf("rest/api/3/issue/%s/comment", issueKey)

	reqBody := map[string]interface{}{
		"body": map[string]interface{}{
			"version": 1,
			"type":    "doc",
			"content": []map[string]interface{}{
				{
					"type":  "heading",
					"attrs": map[string]interface{}{"level": 3},
					"content": []map[string]interface{}{
						{
							"type": "text",
							"text": heading,
						},
					},
				},
				{
					"type": "paragraph",
					"content": []map[string]interface{}{
						{
							"type": "text",
							"text": paragraph,
						},
					},
				},
			},
		},
	}

	req, err := c.jiraClient.NewRequest("POST", path, reqBody)
	if err != nil {
		return err
	}

	_, err = c.jiraClient.Do(req, nil)
	return err
}

func makeCustomFieldsContainer() tcontainer.MarshalMap {
	customFieldsContainer := tcontainer.NewMarshalMap()
	return customFieldsContainer
}
