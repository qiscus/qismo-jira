package router

import (
	"net/http"

	"github.com/gin-contrib/requestid"
	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"

	"qismo-jira/api"
	"qismo-jira/config"
	"qismo-jira/middeware"
)

func NewRouter(conf *config.Config) *gin.Engine {
	r := gin.New()

	r.Use(requestid.New())
	r.Use(middeware.HTTPReqLog())

	r.Use(gin.CustomRecovery(func(c *gin.Context, recovered interface{}) {
		if err, ok := recovered.(string); ok {
			log.Error().Msg(err)
		}

		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong on our side",
		})

		c.AbortWithStatus(http.StatusInternalServerError)
	}))

	httpAPI := api.NewAPI(conf)

	r.GET("/", httpAPI.Home)
	r.GET("/internal/healthcheck", httpAPI.Healthcheck)
	r.POST("/w/custom-button", httpAPI.HandleCustomButtonWebhook)
	r.POST("/w/mark-as-resolved", httpAPI.HandleMarkAsResolvedWebhook)

	r.NoRoute(httpAPI.RouteNotFound)

	return r
}
