package qismo

import (
	"fmt"
	"net/http"
	"regexp"
	"strings"

	"github.com/go-resty/resty/v2"
)

const (
	Url       = "https://multichannel.qiscus.com"
	SdkUrl    = "https://api.qiscus.com"
	UserAgent = "qismo-jira"
)

type Client struct {
	AppId     string
	SecretKey string
	Url       string
	SdkUrl    string

	resty *resty.Client
}

type Result struct {
	Data Data `json:"data"`
}
type UserProperties struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
type Extras struct {
	UserProperties []UserProperties `json:"user_properties"`
}
type Data struct {
	Extras *Extras `json:"extras,omitempty"`
}

func New(appId, secretKey, url, sdkUrl string) *Client {
	if url == "" {
		url = Url
	}

	if sdkUrl == "" {
		sdkUrl = SdkUrl
	}

	client := new(Client)
	client.AppId = appId
	client.SecretKey = secretKey
	client.Url = url
	client.SdkUrl = sdkUrl

	client.resty = resty.New()
	client.resty.SetBaseURL(client.Url)
	client.resty.SetHeader("User-Agent", UserAgent)
	client.resty.SetHeader("Content-Type", "application/json")

	client.resty.SetHeader("Qiscus-App-Id", client.AppId)
	client.resty.SetHeader("Qiscus-Sdk-App-Id", client.AppId) // for sdk request

	client.resty.SetHeader("Qiscus-Secret-Key", client.SecretKey)
	client.resty.SetHeader("Qiscus-Sdk-Secret", client.SecretKey) // for sdk request

	return client
}

func (c *Client) UpdateRoomAdditionalInfo(roomId string, userProperties []UserProperties) error {
	result := new(Result)

	resp, err := c.resty.SetBaseURL(c.Url).R().
		SetResult(result).
		SetPathParam("RoomId", roomId).
		Get("/api/v1/qiscus/room/{RoomId}/user_info")

	if err != nil {
		return err
	}

	if resp.StatusCode() != http.StatusOK {
		return fmt.Errorf("failed to get current additional info. code %v. body %s", resp.StatusCode(), string(resp.Body()))
	}

	if result.Data.Extras != nil && len(result.Data.Extras.UserProperties) >= 1 {
		userProperties = append(userProperties, result.Data.Extras.UserProperties...)
	}

	resp, err = c.resty.SetBaseURL(c.Url).R().
		SetBody(map[string]interface{}{"user_properties": userProperties}).
		SetPathParam("RoomId", roomId).
		Post("/api/v1/qiscus/room/{RoomId}/user_info")

	if err != nil {
		return err
	}

	if resp.StatusCode() != http.StatusOK {
		return fmt.Errorf("failed to set additional info. code %v. body %s", resp.StatusCode(), string(resp.Body()))
	}

	return nil
}

type LoadCommentsWithRangeResult struct {
	Results Results `json:"results"`
	Status  int     `json:"status"`
}
type SdkExtras struct {
	AdditionalExtras interface{} `json:"additional_extras"`
	IsCustomer       bool        `json:"is_customer"`
	Type             string      `json:"type"`
}
type User struct {
	Active    bool      `json:"active"`
	AvatarURL string    `json:"avatar_url"`
	Extras    SdkExtras `json:"extras"`
	UserID    string    `json:"user_id"`
	Username  string    `json:"username"`
}
type CommentExtras struct {
	UserProperties []UserProperties `json:"user_properties"`
	Action         string           `json:"action"`
}
type Comment struct {
	Extras    CommentExtras `json:"extras"`
	ID        int           `json:"id"`
	Message   string        `json:"message"`
	Timestamp string        `json:"timestamp"`
	Type      string        `json:"type"`
	User      User          `json:"user"`
}
type Results struct {
	Comments []Comment `json:"comments"`
}

func (c *Client) LoadAllCommentsInRoom(roomId, lastCommentId string) ([]Comment, error) {
	var result LoadCommentsWithRangeResult

	resp, err := c.resty.
		SetBaseURL(c.SdkUrl).
		R().
		SetQueryParam("room_id", roomId).
		SetQueryParam("first_comment_id", "0").
		SetQueryParam("last_comment_id", lastCommentId).
		SetResult(&result).
		Get("/api/v2.1/rest/load_comments_with_range")

	if err != nil {
		return nil, err
	}

	if resp.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("failed to load comments. code %v. body %s", resp.StatusCode(), string(resp.Body()))
	}

	return result.Results.Comments, err
}

func (c *Client) BuildMessagesForExport(comments []Comment) string {
	strComment := ""

	for _, comment := range comments {
		timestamp := comment.Timestamp
		if timestamp == "" {
			timestamp = "unknown timestamp"
		}

		username := comment.User.Username
		if username == "" {
			username = "unknown username"
		}

		role := comment.User.Extras.Type
		if role == "" {
			role = "unknown user type"
		}

		if comment.Type == "system_event" {
			role = "system"
		}

		if strings.HasSuffix(comment.User.UserID, "_admin@qismo.com") {
			role = "admin"

			if comment.Extras.Action == "bot_reply" {
				role = "chatbot"
				username = "Chatbot"
			}
		}

		message := comment.Message
		if message == "" {
			message = "unknown message"
		}

		space := regexp.MustCompile(`\s+`)
		message = space.ReplaceAllString(message, " ")

		exportedMessage := fmt.Sprintf("[%s] %s (%s): %s\n", timestamp, username, role, message)
		strComment = strComment + exportedMessage
	}

	return strComment
}

func (c *Client) PostCustomSystemEventMessage(roomId, message string) error {
	body := map[string]interface{}{
		"room_id":           roomId,
		"system_event_type": "custom",
		"message":           message,
	}

	resp, err := c.resty.SetBaseURL(c.SdkUrl).R().SetBody(body).Post("/api/v2/rest/post_system_event_message")
	if err != nil {
		return err
	}

	if resp.StatusCode() != http.StatusOK {
		return fmt.Errorf("failed to post system event message. code: %v. body: %s", resp.StatusCode(), string(resp.Body()))
	}

	return nil
}

func (c *Client) ExportRoomChatHistories(roomId string) (string, error) {
	type Result struct {
		Data struct {
			DownloadURL string `json:"download_url"`
		} `json:"data"`
	}

	result := new(Result)

	resp, err := c.resty.SetBaseURL(c.Url).R().
		SetQueryParam("room_id", roomId).
		SetResult(result).
		Get("/api/v1/export/conversations")

	if err != nil {
		return "", err
	}

	if resp.StatusCode() != http.StatusOK {
		return "", fmt.Errorf("failed to get conversation history. code %v. body %s", resp.StatusCode(), string(resp.Body()))
	}

	if result.Data.DownloadURL == "" {
		return "", fmt.Errorf("download_url in response body is null")
	}

	resp, err = c.resty.R().Get(result.Data.DownloadURL)
	if err != nil {
		return "", err
	}

	return string(resp.Body()), nil
}

func (c *Client) GetDashboardLinkForRoomId(roomId string) string {
	return fmt.Sprintf("%s/web/%s/inbox#id=%s", c.Url, c.AppId, roomId)
}
